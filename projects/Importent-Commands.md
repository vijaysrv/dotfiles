
# vaultwarden deploy with letsencrypt

```
docker run -d --name bitwarden --restart unless-stopped -v /bw-data:/data -v /etc/letsencrypt/archive/www.linuxnerd.xyz/:/ssl -e ROCKET_TLS='{certs="/ssl/fullchain1.pem",key="/ssl/privkey1.pem"}' -p 8080:80 vaultwarden/server:latest
```

## Reference

* For Walkthrough Guid https://www.youtube.com/watch?v=eCJA1F72izc
* For Let's Encrypt https://www.youtube.com/watch?v=PGDx3xxLGgA
* install Let's Encrypt https://certbot.eff.org/instructions?ws=apache&os=debianbuster


# Cloneing dotfiles

Clone Repo (https):
```
git clone --bare https://gitlab.com/vijaysrv/dotfiles.git $HOME/.local/share/dotfiles
```
Clone Repo (git):
```
git clone --bare git@gitlab.com:vijaysrv/dotfiles.git $HOME/.local/share/dotfiles
```
Add dotfile alias to .bashrc or .zshrc:
```
alias dotfile='git --git-dir=$HOME/.local/share/dotfiles --work-tree=$HOME'
```
Ignore unnecessary files (IMPORTANT):
```
dotfile config --local status.showUntrackedFiles no
```
To stop README.md file from being downloaded (Optional):
```
dotfile config core.sparsecheckout true
touch $HOME/.config/dotfiles/info/sparse-checkout
echo '/*'>>$HOME/.local/share/dotfiles/info/sparse-checkout
echo '!README.md'>>$HOME/.local/share/dotfiles/info/sparse-checkout
```
Replace existing files with the files from the repo:
```
dotfile checkout -f
```
