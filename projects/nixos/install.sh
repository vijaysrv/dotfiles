#!/bin/bash
#
# Structure for the disc
# - partion 1 => Boot
# - partition 2 => LVM (LUKS Container)
#    cryptroot
#   - lvmvg-swap - swap
#   - lvmvg-root - system
#
# e.g.
# $ lsblk
# NAME                 MAJ:MIN RM  SIZE RO TYPE  MOUNTPOINT
# nvme0n1              259:0    0  1.8T  0 disk
# ├─nvme0n1p1          259:1    0    1G  0 part  /boot
# └─nvme0n1p2          259:2    0  1.8T  0 part
#   └─root             254:0    0  1.8T  0 crypt
#     ├─nixos--vg-swap  254:1    0   16G  0 lvm   [SWAP]
#     └─nixos--vg-root 254:2    0  1.8T  0 lvm   /

set -o errexit
set -o nounset
set -o pipefail

# Use lsblk - to show available discs
# Set!
if [ -z $1 ]; then
	exit 1
fi
readonly DISK=/dev/$1  # e.g /dev/sda or /dev/nvme0n1
readonly SWAP_SIZE=512M
# readonly LUKS_PASSWORD="XXX" # TODO - does not work

readonly BOOT_PARTITION=${DISK}1
readonly LVM_PARTITION=${DISK}2
readonly SYSTEM_VOLUME=${DISK}2p2 # maybe needs adapation
readonly TMP_CONFIG_PATH=/mnt/etc/nixos/configuration.nix

main(){

    if [ ${#@} -eq 0 ]
    then
	print_help
    else for FUNCTION in "$@"
	 do
	     "${FUNCTION}"
	 done
    fi
}

print_help(){
    echo "Specify function to call - options:"
    echo "- generate_partitions"
    echo "- generate_luks_volume_and_format"
    echo "- mount_fs_and_generate_config"
    echo "- adapt_config"
    echo "- install_nixos"
    echo ""
    echo "or use 'all' to run all the functions"
    echo ""
    echo "Make sure the variable in the script are properly set!"
}

all(){
    generate_partitions
    generate_luks_volume_and_format
    mount_fs_and_generate_config
    adapt_config
    install_nixos
}

generate_partitions(){

    echo "Generate partitions"

    # Remove previous partitions
    wipefs -af "$DISK"
    sgdisk -Zo "$DISK"

    # Important for understanding this : For the tool sgdisk the "0"
    # has a special meaning as described here:
    # https://fedoramagazine.org/managing-partitions-with-sgdisk/
    # - partition number field: 0 is placeholder for next available
    #   number (starts at 1)
    # - starting address field: 0 start of the largest available block
    #   of free space
    # - ending address field: 0 is placeholder for the end of the
    #   largest available block of free

    # Create EFI boot partition
    sgdisk -n 0:0:+1G -t 0:EF00 ${DISK}

    # Create LVM partition
    sgdisk -n 0:0:0 -t 0:8e00 ${DISK}
}

generate_luks_volume_and_format(){

    echo "Generate LUKS volumes and format them"

    # Generate keys for single password unlock
    dd if=/dev/urandom of=./keyfile.bin bs=1024 count=4

    # Generatee LUKS volume
    ### TODO: Use password stored in a variable
    ### echo "${LUKS_PASSWORD}" | cryptsetup luksFormat ${LVM_PARTITION} - # Does not work
    cryptsetup -c aes-xts-plain64 -s 256 -h sha512 luksFormat ${LVM_PARTITION}

    # Decrypt the encrypted partition and call it Jarvis. The
    # decrypted partition will get mounted at /dev/mapper/Jarvis
    ### TODO: Use password stored in a variable
    ### echo "${LUKS_PASSWORD}" | cryptsetup luksOpen ${LVM_PARTITION} Jarvis - # Does not work
    cryptsetup luksOpen ${LVM_PARTITION} Jarvis -d keyfile.bin

    # Create the LVM physical volume using Jarvis
    pvcreate /dev/mapper/Jarvis

    # Create a volume group that will contain our root and swap partitions
    vgcreate Jarvis-vg /dev/mapper/Jarvis

    # Create a swap partition labeled "swap"
    lvcreate -L ${SWAP_SIZE} -n swap Jarvis-vg

    # Create a logical volume labeled "root" for the root filesystem
    # using the remaining free space.
    lvcreate -l "100%FREE" -n root Jarvis-vg

    # Format boot partition with fat32
    mkfs.vfat -n boot ${BOOT_PARTITION}

    # Format root partition as ext4
    mkfs.ext4 -L nixos /dev/Jarvis-vg/root

    # Create swap file system
    mkswap -L swap /dev/Jarvis-vg/swap

    # Switch swap on
    swapon /dev/Jarvis-vg/swap

}

mount_fs_and_generate_config(){

    echo "Mount file system and generater configuration file"

    mount /dev/Jarvis-vg/root /mnt
    mkdir -p /mnt/boot/efi
    mount $BOOT_PARTITION /mnt/boot
    nixos-generate-config --root /mnt
}

adapt_config(){
    # Makes some very minor adaptions to the configuration
    # file. Should later be further extended after installation has
    # finished.

    echo "Adapting ${TMP_CONFIG_PATH}"

    UUID=`lsblk -o UUID ${LVM_PARTITION} | sed -n 2p`
    UUID=`lsblk -o UUID ${BOOT_PARTITION} | sed -n 2p`
    cat << EOF > ${TMP_CONFIG_PATH}
{ config, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
    ];

  boot.loader.efi.canTouchEfiVariables = true;
  boot.loader.efi.efiSysMountPoint = "/boot/efi";
  boot.loader.grub = {
    enable = true;
    device = "nodev";
    version = 2;
    efiSupport = true;
    enableCryptodisk = true;
   };
   # LVM needs to be descrypted
  boot.initrd.luks.devices = {
   root = {
     device =  "/dev/disk/by-uuid/${UUID1}";
     preLVM = true;
	 allowDiscards = true;
     keyFileSize = 4096;
     # pinning to /dev/disk/by-id/usbkey works
     # keyFile = "/dev/disk/by-uuid/${UUID2}"
    };
    };

    networking.hostName = "Jarvis"; # Define your hostname.
    networking.networkmanager.enable = true;
    networking.useDHCP = false;
    networking.interfaces.enp2s0.useDHCP = true;
    networking.interfaces.wlp4s0.useDHCP = true;

    time.timeZone = "Asia/Kolkata";

    # List packages installed in system profile. To search, run:
    environment.systemPackages = with pkgs; [
      vim wget htop firefox alacritty picom
    ];

    users.extraUsers.tony = {
       createHome = true;
       extraGroups = ["wheel" "video" "audio" "disk" "networkmanager"];
       group = "users";
       description = "Vijayakumar Ravi";
       home = "/home/tony";
       isNormalUser = true;
       uid = 1000;
       hashedPassword = "\$6\$jaP29S8fvIB9QB.A\$0jliOXqE6vD4ZvPYsXCzdkyv6deyr/2gzlhsQy/whGbNEdVfbEOFp5GcgNUsx.QtAgvQByyUmeBLbWszuHkHv1";
    };

	services.xserver = {
	   enable = true;
	   autorun = true;
	   layout = "us";

	   desktopManager = {
               xterm.enable = false;
               pantheon.enable = true;
	   };

	   displayManager = {
               defaultSession = "pantheon";
	   };

	   windowManager.i3 = {
              enable = true;
	   };

	   windowManager.dwm = {
               enable = true;
	   };
};

}
EOF
}

install_nixos(){

    echo "Install NixOS"

    nixos-install
    echo "Done - will reboot in 10 sec"
    sleep 10
    #reboot
}

main "$2"
