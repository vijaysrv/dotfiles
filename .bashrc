# Syntax highlighting ble.sh
[[ $- == *i* ]] &&
  source "$HOME/.local/share/blesh/ble.sh" --attach=none

# Randomly insults the user when typing wrong command.
[ -f /etc/bash.command-not-found ] && . /etc/bash.command-not-found

#auto cd
shopt -s autocd
shopt -s cdspell
shopt -s cdable_vars
shopt -s expand_aliases
shopt -s histappend
shopt -s checkwinsize

# vim mode
set -o vi

export HISTCONTROL=ignoreboth:erasedups
export VISUAL="/usr/bin/nvim"
export EDITOR="$VISUAL"

source ~/.config/shell/aliasrc
source ~/.git-prompt.sh ~/.git-completion.bash
source ~/.profile

# Man page with vim
#  export MANPAGER="/bin/sh -c \"unset PAGER;col -b -x | \
#      vim -R -c 'set ft=man norelativenumber nolist' \
#      -c 'map q :q<CR>' \
#      -c 'map <SPACE> <C-D>' -c 'map b <C-U>' \
#      -c 'nmap K :Man <C-R>=expand(\\\"<cword>\\\")<CR><CR>' -\""
#  if shopt -q login_shell; then
#  	echo -e "\e[0;33mVanakkam da Mapla\e[0;31m\e[0;32m \n"
#  	date
#  	function _exit()              # Function to run upon exit of shell.
#  	{
#  		echo -e "\e[0;31mPoittu varan da Iyyasammi!!!\e[m"
#  	}
#  trap _exit EXIT
#  fi
fortune | cowsay -n
# PS1="\[\033[32m\]\w\n\e[0;38m[\e[0;35m\u\e[1;36m@\e[0;38m\h\e[0;35m]\e[1;36m ∮ \[\033[0m\]"
# PS1="\n🍎\e[0;34m \033[1m$(ifconfig $(route -n | grep ^0.0.0.0 | awk '{print $NF}') | grep inet | grep -v inet6 | awk '{print $2}') (\W)$(__git_ps1 " [%s]") : \033[0m"

force_color_prompt=yes

if [ -n "$force_color_prompt" ]; then
    if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
	# We have color support; assume it's compliant with Ecma-48
	# (ISO/IEC-6429). (Lack of such support is extremely rare, and such
	# a case would tend to support setf rather than setaf.)
	color_prompt=yes
    else
	color_prompt=
    fi
fi

if [ "$color_prompt" = yes ]; then
    prompt_color='\[\033[;32m\]'
    info_color='\[\033[1;34m\]'
    prompt_symbol=📛
    if [ "$EUID" -eq 0 ]; then # Change prompt colors for root user
	prompt_color='\[\033[;94m\]'
	info_color='\[\033[1;31m\]'
	prompt_symbol=💀
    fi
    PS1=$prompt_color'┌──${debian_chroot:+($debian_chroot)──}('$info_color'\u${prompt_symbol}\h'$prompt_color')-[\[\033[0;1m\]\w'$prompt_color']\n'$prompt_color'└─'$info_color'\$\[\033[0m\] '
    # BackTrack red prompt
    #PS1='${debian_chroot:+($debian_chroot)}\[\033[01;31m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
else
    PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
fi
unset color_prompt force_color_prompt

# Handy Extract Program
function extract()
{
    if [ -f $1 ] ; then
        case $1 in
            *.tar.bz2)   tar xvjf $1     ;;
            *.tar.gz)    tar xvzf $1     ;;
            *.bz2)       bunzip2 $1      ;;
            *.rar)       unrar x $1      ;;
            *.gz)        gunzip $1       ;;
            *.tar)       tar xvf $1      ;;
            *.tbz2)      tar xvjf $1     ;;
            *.tgz)       tar xvzf $1     ;;
            *.zip)       unzip $1        ;;
            *.Z)         uncompress $1   ;;
            *.7z)        7z x $1         ;;
            *)           echo "'$1' cannot be extracted via >extract<" ;;
        esac
    else
        echo "'$1' is not a valid file!"
    fi
}

# install Aur packages
aur(){ curl  -O https://aur.archlinux.org/cgit/aur.git/snapshot/$1.tar.gz && tar -xvf $1.tar.gz && cd $1 && makepkg --noconfirm -si && cd .. && rm -rf $1 $1.tar.gz ;}
# syntax highlighting
((_ble_bash)) && ble-attach

countdown(){
    date1=$((`date +%s` + $1));
    while [ "$date1" -ge `date +%s` ]; do 
    ## Is this more than 24h away?
    days=$(($(($(( $date1 - $(date +%s))) * 1 ))/86400))
    echo -ne "$days day(s) and $(date -u --date @$(($date1 - `date +%s`)) +%H:%M:%S)\r"; 
    sleep 0.1
    done
}
stopwatch(){
    date1=`date +%s`; 
    while true; do 
    days=$(( $(($(date +%s) - date1)) / 86400 ))
    echo -ne "$days day(s) and $(date -u --date @$((`date +%s` - $date1)) +%H:%M:%S)\r";
    sleep 0.1
    done
}
. "$HOME/.cargo/env"
