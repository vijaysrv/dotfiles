# setup font settings
font_family  JetBrains Mono
font_size 14.0

# no bells. Ever.
enable_audio_bell no
bell_on_tab no

# default layout is vertical splits only
enabled_layouts splits

# don't draw extra borders, but fade the inactive text a bit
active_border_color none
inactive_text_alpha 0.6

# tabbar should be at the top
tab_bar_edge top
tab_bar_style separator
tab_separator " ┇"

update_check_interval 24
startup_session default-session.conf
map ctrl+shift+q noop

# open new split (window) with Alt+d retaining the cwd
map Alt+d new_window_with_cwd

# open new tab with Alt+t
map Alt+t new_tab_with_cwd

# new split with default cwd
map Alt+shift+d new_window

# switch between next and previous splits
map Alt+]        next_window
map Alt+[        previous_window

# clear the terminal screen
map Alt+k combine : clear_terminal scrollback active : send_text normal,application \x0c

# jump to beginning and end of word
map Alt+left send_text all \x1b\x62
map Alt+right send_text all \x1b\x66

# jump to beginning and end of line
map Alt+Shift+left send_text all \x01
map Alt+Shift+right send_text all \x05

# Map Alt + <num> to corresponding tabs
map Alt+1 goto_tab 1
map Alt+2 goto_tab 2
map Alt+3 goto_tab 3
map Alt+4 goto_tab 4
map Alt+5 goto_tab 5
map Alt+6 goto_tab 6
map Alt+7 goto_tab 7
map Alt+8 goto_tab 8
map Alt+9 goto_tab 9

# changing font sizes
map Alt+equal    change_font_size all +2.0
map Alt+minus    change_font_size all -2.0
map Alt+0        change_font_size all 0

# copy_to_clipboard
map Alt+c copy_to_clipboard 
map Alt+v paste_from_clipboard  

#background opacity  
background_opacity 0.8

#FrontEndDelight Theme
# https://github.com/dexpota/kitty-themes/blob/master/themes/FrontEndDelight.conf
background #1b1b1d
foreground #acacac
cursor #cccccc
selection_background #e96153
color0 #242426
color8 #5eac6c
color1 #f8501a
color9 #f64319
color2 #565746
color10 #74eb4c
color3 #f9761d
color11 #fcc224
color4 #2c70b7
color12 #3393c9
color5 #f02d4e
color13 #e75e4e
color6 #3ba0a5
color14 #4ebce5
color7 #acacac
color15 #8b735a
selection_foreground #1b1b1d
