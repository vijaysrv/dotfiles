#
# ~/.bash_profile
#
#export PATH=$PATH:/home/vijay/.local/bin
export PATH="${PATH}:${HOME}/.local/bin/"

[[ -f ~/.bashrc ]] && . ~/.bashrc

if [[ -z $DISPLAY ]] && [[ $(tty) = /dev/tty1 ]]; then
    startx
elif [[ -z $DISPLAY ]] && [[ $(tty) = /dev/tty2 ]]; then
    startx
elif [[ -z $DISPLAY ]] && [[ $(tty) = /dev/tty3 ]]; then
    startx
fi
#. "$HOME/.cargo/env"
