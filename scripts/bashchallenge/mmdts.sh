#!/bin/bash
############### My Dots to Repo Converter ################
# Author: Matthew A. Weber AKA The Linux Cast
# Version: 1.1
# First Version Released 08-05-2021
# Last Edited: 08-07-2021
# GPL V3
# Desc: Moves dotfiles from .config folder to cust om repo, then creates symlinks back to .config
# Currently Supported Dots: Alacritty, BSPWM, i3, AwesomeWM
##########################################################

#Define Repo Location. If you would like to customize the location or name of your repo, do so here.
repo=$HOME/myrepo

#Define Colors
red='\033[0;31m'
nc='\033[0m' #Back in Black

#DO Warning about config backup

printf "

     ${red}WARNING!${nc} 

     This script will ${red}move${nc} directories from your .config directory 
     into a directory called ${red}myrepo${nc} in your HOME directory.
     
     It does attempt to make a backup. The backup file will be located in your 
     HOME Directory if it is needed.

     "

read -p "Are You Sure You'd Like to Continue? [Y/n] " input
 
case $input in
    [yY][eE][sS]|[yY])
     echo "Yes" ;;
    [nN][oO]|[nN])
    exit ;;
    *)
    echo "Invalid input..."
    exit 1
 ;;
esac

#Check if config file exists.
if [ -d "$HOME/.config" ]; then
    printf ".config file exists, script will continue \n"
else
    exit 1
fi

#Make Repo
if [ ! -f $repo ]; then
    mkdir $repo
else
    printf "Repo Already Exists\n"
fi

#Backup Congig file if Config File exists
if [ -d "$HOME/.config/" ]; then
    cp -Rf ~/.config ~/.config-backup-$(date +%Y.%m.%d-%H.%M.%S)
else
    printf "Where is your .config file, you hooligan?\n"
    exit
fi

#BSPWM
if [ -d "$HOME/.config/bspwm" ]; then
    mv $HOME/.config/bspwm "$repo" && ln -s "$repo"/bspwm $HOME/.config/bspwm
else
    echo "no bspwm"
fi

#i3wm
if [ -d "$HOME/.config/i3" ]; then
    mv $HOME/.config/i3 "$repo" && ln -s "$repo"/i3 $HOME/.config/i3
else
    echo "no i3"
fi

#awesome
if [ -d "$HOME/.config/awesome" ]; then
    mv $HOME/.config/awesome "$repo" && ln -s "$repo"/awesome $HOME/.config/awesome
else
    echo "no bspwm"
fi

#alacritty
if [ -d "$HOME/.config/alacritty" ]; then
    mv $HOME/.config/alacritty "$repo" && ln -s "$repo"/alacritty $HOME/.config/alacritty
else
    echo "no alacritty"
fi

#neofetch
if [ -d "$HOME/.config/neofetch" ]; then
    mv $HOME/.config/neofetch "$repo" && ln -s "$repo"/neofetch $HOME/.config/neofetch
else
    echo "no neofetch"
fi

#qtile
if [ -d "$HOME/.config/qtile" ]; then
    mv $HOME/.config/qtile "$repo" && ln -s "$repo"/qtile $HOME/.config/qtile
else
    echo "no qtile"
fi

#qutebrowser
if [ -d "$HOME/.config/qutebrowser" ]; then
    mv $HOME/.config/qutebrowser "$repo" && ln -s "$repo"/qutebrowser $HOME/.config/qutebrowser
else
    echo "no qutebrowser"
fi

#ranger
if [ -d "$HOME/.config/ranger" ]; then
    mv $HOME/.config/ranger "$repo" && ln -s "$repo"/ranger $HOME/.config/ranger
else
    echo "no ranger"
fi

#zsh
if [ -d "$HOME/.config/zsh" ]; then
    mv $HOME/.config/zsh "$repo" && ln -s "$repo"/zsh $HOME/.config/zsh
else
    echo "no zsh"
fi

#herbstluftwm
if [ -d "$HOME/.config/herbstluftwm" ]; then
    mv $HOME/.config/herbstluftwm "$repo" && ln -s "$repo"/herbstluftwm $HOME/.config/herbstluftwm
else
    echo "no herbstluftwm"
fi

#spectrwm
if [ -d "$HOME/.config/spectrwm" ]; then
    mv $HOME/.config/spectrwm "$repo" && ln -s "$repo"/spectrwm $HOME/.config/spectrwm
else
    echo "no spectrwm"
fi

#picom
if [ -d "$HOME/.config/picom" ]; then
    mv $HOME/.config/picom "$repo" && ln -s "$repo"/picom $HOME/.config/picom
else
    echo "no picom"
fi

#mpd
if [ -d "$HOME/.config/mpd" ]; then
    mv $HOME/.config/mpd "$repo" && ln -s "$repo"/mpd $HOME/.config/mpd
else
    echo "no mpd"
fi

#ncmpcpp
if [ -d "$HOME/.config/ncmpcpp" ]; then
    mv $HOME/.config/ncmpcpp "$repo" && ln -s "$repo"/ncmpcpp $HOME/.config/ncmpcpp
else
    echo "no ncmpcpp"
fi

printf "
    If the script succeeded, delete the backup of your config file, 
    as it does take up a lot of HDD space. 

    All That's left to do is create your git repo online and git init"
