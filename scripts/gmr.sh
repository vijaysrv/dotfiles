#!/bin/bash

# Git script for my dot files
# Author: Matthew Weber AKA The Linux Cast
# Version 1.0
# Created 11/20/21 
# Last Updated: 11/20/21

#####################################################

repo='/home/drmdub/myrepo'
echo "What Commit Message Would You Like to Use?"
read -e -p "Message:" msg


git -C $repo add --all && git -C $repo commit -m "$msg" && git -C $repo push origin master



