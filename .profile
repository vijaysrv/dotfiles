#USER SETTINGS
export PATH="${PATH}:${HOME}/.local/bin/:${HOME}/scripts"
#[[ -f ~/.bashrc ]] && . ~/.bashrc

if [ -z "${DISPLAY}" ] && [ "${XDG_VTNR}" -eq 1 ]; then
  exec startx
fi

if [[ -z $DISPLAY ]] && [[ $(tty) = /dev/tty1 ]]; then
    startx
elif [[ -z $DISPLAY ]] && [[ $(tty) = /dev/tty2 ]]; then
    startx
elif [[ -z $DISPLAY ]] && [[ $(tty) = /dev/tty3 ]]; then
    startx
elif [[ -z $DISPLAY ]] && [[ $(tty) = /dev/tty4 ]]; then
    startx
fi

. "$HOME/.cargo/env"
